class AddLockVersionToAuthor < ActiveRecord::Migration[5.2]
  def change
    add_column :authors, :lock_version, :integer
  end
end
