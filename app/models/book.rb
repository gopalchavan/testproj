class Book < ApplicationRecord
  belongs_to :author
  enum status: [ :active, :archived ]
end
