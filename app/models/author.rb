class Author < ApplicationRecord
  before_destroy :check
  has_many :books, dependent: :destroy
  def check
    puts self.books
  end
end
