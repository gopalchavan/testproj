class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.string :name
      t.decimal :cost
      t.belongs_to :author
      t.integer :books_count
      # , foreign_key: true

      t.timestamps
    end
  end
end
